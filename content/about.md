---
title: "About The LackAStack Team"
type: pages
layout: page
---

## Chris Timberlake
[Chris Timberlake](https://linkedin.com/in/ctimberlake/) comes to LackAStack
with 10+ Years of industry experience spanning from Web Development, Mobile
Development, CI/CD, DevOps, Security, and IT Administration. Currently he works
with GitLab in the Professional Services Department. Previously he has done
consulting work with Red Hat and Amazon with numerous Fortune 500 Companies as
clients.

## Michael Langfermann
[Michael Langfermann](https://www.xing.com/profile/Michael_Langfermann2/cv)
joined LackAStack because a co-worker suggested he should write a book based on
his expertise. He started out with an education in electronic engineering and
moved into the IT space due to his passion. He has experience more 10+ years in
IT, native Application Development, Web Development and Cloud Infrastructure.
Currently he helms a E-commerce project with several, close knit devs that
follow his lead.
