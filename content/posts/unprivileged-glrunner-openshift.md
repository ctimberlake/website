---
title: "Deploying a GitLab Runner in OpenShift Unprivileged"
description: "GitLab Runner in OpenShift without AnyUID and other security restrictions."
date: 2019-11-21T13:52:22+02:00
author: Chris Timberlake
authorURL: "http://linkedin.com/in/ctimberlake"
authorAvatar: "/img/avatar/chris-timberlake.jpeg"
topic: "OpenShift"
category: "Development"
categories: ["Development", "OpenShift", "GitLab", "Docker"]
---

## Requirements
- One OpenShift Cluster (With Storage)
- One GitLab Installation
- Cluster Admin access to OpenShift Cluster

## Introduction

By default GitLab & it's Runners require anyuid and root access to work. They also require Helm charts to install. If you're an OpenShift Administrator, these two things are likely not compatible with your way of doing things. You likely also dont want GitLab Runners to run CI Jobs using AnyUID and Root access. The purpose of this guide is to walk you through how to build your own GitLab Runner container that does not use Root or AnyUID. So that it's secure and also communicated properly with GitLab.

The very first thing you're going to want to do is clone the repo with our OpenShift Resources. Then we'll go through them.

{{< highlight bash "linenos=table" >}}
cd <Your_Projects_Directory>
git clone https://gitlab.com/lackastack/article-repos/openshift-gitlab-runner.git
{{< / highlight >}}



## How the runner is assembled.

I've gone through the process of setting up OpenShift Templates to make this process easier. Without it, this guide may be super long. I encourage you to read through the templates and understand how they work and what they do. I'm also not going to deeply dive into the dockerfile, but call out some specific things.

Go ahead and open the Dockerfile. This is the file we're going to be using for building our container. From lines 6 - 27, You'll see that we're inserting the GitLab Certificates for YUM Repos. Then we're going through on line 29-31 and installing the gitlab-runner. Everytime you build this container, it will install the newest gitlab runner into the container.

But it's lines 34-40 I want to point out.

{{< highlight dockerfile "linenos=table" >}}
RUN mkdir -p /etc/gitlab-runner/certs && \
    mkdir /.gitlab-runner && \
    mkdir /tmp/gitlab-home && \
    chmod -R g=u /.gitlab-runner && \
    chmod -R g=u /etc/gitlab-runner && \
    chmod -R g=u /tmp/gitlab-home && \
    chmod +x /usr/bin/dumb-init
{{< / highlight >}}

These lines do the magic. When the gitlab-runner installs via yum, it creates folders itself with permissions for root and the gitlab user. Because OpenShift runs as random uid, we can't/shouldn't use these. So we're going ahead and making new folders and also reassigning those folders to the root group. You see, when OpenShift assigns a random uid to a container, that random uid is always in the root group. So because we use ```chmod -r g=u``` this command removes the user permissions to those folders and assigns them to a group.

Now, let's go ahead and open the entrypoint file. Scroll down to lines 19-31, Ignore that certificate stuff.

{{< highlight bash "linenos=table" >}}
if [ ! -f "/.gitlab-runner/config.toml" ]; then
  gitlab-runner register --non-interactive \
                        --template-config /scripts/config.toml \
                        --url $GITLAB_URL \
                        --executor kubernetes \
                        --locked=false \
                        --access-level="not_protected" \
                        --docker-privileged=false \
                        --kubernetes-privileged=false 
fi
# Start the runner
gitlab-runner run --working-directory=/tmp/gitlab-home --listen-address=0.0.0.0
{{< / highlight >}}

Everytime the container starts/restarts, etc will run this script. We start by checking to see if the config.toml is in one of the directories we created. If it's not, then we need to register the runner. So we start that process, we register the runner with a one-line registration method. The secret sauce is here ```--template-config /scripts/config.toml```. Later in our openshift templates, we'll create a configmap with our basic config.toml, and then input our default values into it. The GitLab Runner will take this file, use it as a template and fill out the rest for us.

{{< highlight bash "linenos=table" >}}
--executor kubernetes
{{< / highlight >}}

This is so the runner operates as if it was in Kubernetes, and it sort-of is.

{{< highlight bash "linenos=table" >}}
--access-level="not_protected" \
--docker-privileged=false \
--kubernetes-privileged=false
{{< / highlight >}}

These lines tell the runner to run with the fewest permissions as possible. This prevents us from doing Docker in Docker - But because you're on OpenShift we don't need that.. You'll see why in an upcoming *GitLabOps on OpenShift* tutorial.


## Importing and Building the Runner.

This is where you're Cluster Admin access will come in handy! Before we do this, Please take a look at the `build-template.yml` file. This is what we'll be using and importing. You shouldn't import anything into your cluster without looking into it and what it does. Without further-a-do lets import the template, run the following.

{{< highlight bash "linenos=table" >}}
oc login <stuff>
oc project openshift
oc process -f ./build-template.yml | oc apply -f-
{{< / highlight >}}

This will import the build object and image stream into your OpenShift cluster in the OpenShift Namespace. Why there? Because we want this image and template to be accessible by everyone, but not editable by anyone but a cluster admin. Now that this is done, let's go ahead and start the image build.

{{< highlight bash "linenos=table" >}}
cd <Your_GitCloned_Directory>
oc start-build gitlab-runner --from-dir=. --follow
{{< / highlight >}}

At this point, the build is going to start and stream it's output to the terminal. Let it run it can take up to 5 minutes. When it's done you should see a success or pushing image message. This means the image is in the repo and ready to be used.

## The GitLab Runner Template.

This is the template that we want to import for all users of GitLab to provision and deploy their own runners. The idea is that users of OpenShift and GitLab can have a namespace on OpenShift and a project on GitLab, Then install a runner on OpenShift in their namespace pointed to their GitLab project. Then their runners don't affect anyone elses namespaces, you can monitor metrics per namespace, and limit resources on the namespace also. Plus, it's self-service, so win-win.

The template included is big, Please read it. However, i'm not going to quote it here because it's huge. This template when installed in the OpenShift namespace adds the GitLab-Runner to the self-service portal for namespaces. When executed this template installs:
- 2 Persistent Volume Claims
- 1 Deployment Config
- 1 ConfigMap
- 2 RoleBindings
- 1 Service Account

All of these elements are required to power the runner. Let's go ahead and install it now.

{{< highlight bash "linenos=table" >}}
oc login <stuff>
oc project openshift
oc apply -f ./runner-template.yml -n openshift
{{< / highlight >}}

If this succeeds in a few moments users will be able to see the GitLab-Runner object in their self-service portal.

## The Final Touches, Deploying the Runner.



<div class="row">

  <div class="column-50 padding-20">
  To deploy the runner, you will always need a GitLab URL and GitLab Token. In a GitLab Project/Group go to *Settings -> CI/CD -> Runners*, When there you will see the screenshot below that has your token and GitLab URL to use.
  </div>
    <div class="column-50 padding-20">
    <img alt="GitLab Runner Panel" src="/img/articles/unprivileged-glrunner-openshift/unprivileged-glrunner-openshift-1.png"/>
  </div>
</div>

<div class="row">
  <div class="column-50 padding-20">
    <img alt="GitLab OpenShift Panel" src="/img/articles/unprivileged-glrunner-openshift/unprivileged-glrunner-openshift-2.png"/>
  </div>
  <div class="column-50 padding-20">
  Now in OpenShift, go to the Web UI, Login, then navigate to your project. In the upper right hand corner click "Add To Project". Select "Browse Catalog". Now you'll be treated to a page with a bunch of items, on the tabs at the top click CI/CD and look for GitLab-Runner if it's not there, check the "Other" Tab. Click GitLab-Runner.
  <br/><br/>
  You should now have a screen like below. Fill in the details, hit next, and let er rip. At this point provided everything went down right, GitLab-Runner should deploy, stand itself up, and register then wait for jobs from GitLab.
  </div>
</div>

**If you liked this article, I take payment in [LinkedIn Recommendations and Endorsements.](https://www.linkedin.com/in/ctimberlake)**
