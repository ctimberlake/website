---
title: "Migrating from PFSense to OpenBSD"
date: 2019-08-29T13:52:22+02:00
author: "Michael Langfermann"
authorURL: "https://www.xing.com/profile/Michael_Langfermann2/cv"
---

I have been running {{< ext "PFSense" "https://www.pfsense.org/" >}} for 3
years or so and have been quite happy with it. It's a decent software project
that I encourage you to checkout, if you haven't already. However, after some
neglecting my home infrastructure, it was time to give it some time and love.
Looking through alternatives for PFSense, {{< ext "OPNSense"
"https://www.opnsense.org/" >}}, a fork from PFSense, has been an attractive
option with {{< ext "a much saner architecture"
"https://wiki.opnsense.org/development/architecture.html" >}} than PFSense.

That said, I don't really need a GUI for my router as I am much more fond of
simple configuration files, and a secure edge point to the Internet.  {{< ext
"OpenBSD" "https://www.openbsd.org/" >}} checks of a lot of desired boxes for
an edge router, with it's impressive security track record and networking
features. All I really need is to manage a WAN and a LAN interface, DHCP and
DNS. For wireless I run a TP-Link TL-WDR4300 with {{< ext "OpenWRT"
"https://www.openwrt.org/" >}}, so no need to fiddle around with wireless
networking.

The router itself is in a small form factor case and completely silent. It
sports a Gigabyte GA-N3050N-D3H mainboard with passive cooling, a Intel Celeron
N305 1.6 GHz Dual core, 4 GB RAM, and a 128 GB SSD, which is more than plenty
for a home router.

### Requirements

I need my edge router to do the following:

* WAN to LAN
* DNS for internal services/hosts (e.g. device.lan)
* DHCP with static MAC mapping

